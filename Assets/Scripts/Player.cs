﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class Player : MonoBehaviour {
    [System.Serializable]
    private class Inputs {
        //public float sensitiviy;
        public bool boost;
        public float boostForce;

        public Vector3 move_translation;
        public Vector3 move_rotation;

        public bool cameraControl;

        public Vector3 look_translation;
        public Vector3 look_rotation;

        public bool fire;
        public bool reset;

        public bool menu;

        public bool light;
        public bool lightOff;

        public bool momentumLock;

        public Inputs() {
            //sensitiviy = 0f;

            move_translation = Vector3.zero;
            move_rotation = Vector3.zero;

            cameraControl = false;

            look_translation = Vector3.zero;
            look_rotation = Vector3.zero;

            boost = false;
            boostForce = 0f;
            reset = false;
            fire = false;

            menu = false;

            light = false;
        }

        public void CaptureInput() {
            //sensitiviy = Input.GetAxis("Mouse ScrollWheel");
            //sensitiviy = sensitiviy > 0 ? sensitiviy * 15f : sensitiviy / 15f;
            boost = Input.GetKey(KeyCode.LeftShift) ? true : false;
            boostForce = Input.GetKey(KeyCode.LeftShift) ? 1.5f : 1f;
            if (Input.GetAxis("Vertical") < 0) { boostForce = 1f; }

            move_translation = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Jump"), Input.GetAxis("Vertical") * boostForce) * 10;            
            float spinSpeed = 0.1f;
            if (!cameraControl) { move_rotation = new Vector3(-Input.GetAxis("Pitch"), Input.GetAxis("Yaw"), -Input.GetAxis("Roll")) * spinSpeed; }
            else { look_rotation = new Vector3(-Input.GetAxis("Pitch"), Input.GetAxis("Yaw"), Input.GetAxis("Roll")) * spinSpeed; }

            if (Input.GetKeyDown(KeyCode.Mouse2)) { this.reset = true; /*Debug.Log("CaptureInput(): reset == true");*/ }
            if (Input.GetKeyUp(KeyCode.Mouse2)) { this.reset = false; }
            
            fire = Input.GetMouseButtonDown(0) ? true : false;

            if (Input.GetKeyDown(KeyCode.Tab)) { this.menu = true; /*Debug.Log("menu == true");*/ }
            if (Input.GetKeyUp(KeyCode.Tab)) { this.menu = false; }

            if (Input.GetKeyDown(KeyCode.F)) { this.light = true; /*Debug.Log("light == true");*/ }
            if (Input.GetKeyUp(KeyCode.F)) { 
                this.light = false;
                this.lightOff = !this.lightOff;            
            }

            if (Input.GetMouseButtonDown(1)) { this.cameraControl = true; /*Debug.Log("cameraControl == true");*/ }
            if (Input.GetMouseButtonUp(1)) { this.cameraControl = false; }
        }
    }
    [SerializeField] private Inputs inputs;

    [System.Serializable]
    private class Controls {
        private Inputs inputs;
        private Rigidbody rigidbody;
        private GameObject projectile;
        private List<Weapon> weapons;
        private ParticleSystem trail;
        private Camera camera;

        public Controls(Inputs i, Rigidbody r, GameObject p, List<Weapon> w, ParticleSystem t, Camera c) {
            this.inputs = i;
            this.rigidbody = r;
            this.projectile = p;
            this.weapons = w;
            this.trail = t;
            this.trail.Stop();
            this.camera = c;
        }

        public void Move() {
            this.Move_Translation();
            this.Move_Rotation();
            this.Move_Brake();
        }
        private void Move_Translation() {
            this.rigidbody.AddRelativeForce(inputs.move_translation);
            if (this.inputs.move_translation.z > 0 /*&& this.trail.isStopped*/) { trail.Play(); Debug.Log("trail.Play()"); }
            else if (this.inputs.move_translation.z <= 0) { trail.Stop(); Debug.Log("trail.Stop()"); }
        }
        private void Move_Rotation() {
            this.rigidbody.AddRelativeTorque(inputs.move_rotation);
        }
        private void Move_Brake() {
            //Debug.Log("Move_Brake()");
            if (this.inputs.reset) { 
                this.rigidbody.velocity /= 1.05f;
                if(this.rigidbody.velocity.magnitude < 0.1f) { this.rigidbody.velocity = Vector3.zero; }
                this.rigidbody.angularVelocity /= 1.05f;
                if(this.rigidbody.angularVelocity.magnitude < 0.1f) { this.rigidbody.angularVelocity = Vector3.zero; }
                //Debug.Log("Move_Brake(): inputs.reset == true");
            }
            //else { Debug.Log("Move_Brake(): inputs.reset == false"); }
        }

        public void Attack() {
            if (this.inputs.fire) { this.Shoot(); }
        }
        private void Shoot() {
            foreach(Weapon w in weapons) {
                Vector3 position = w.firePoint.position + w.transform.TransformDirection(new Vector3(0, 0, w.projectile.transform.localScale.z / 2));
                GameObject g = Instantiate(w.projectile, position, w.transform.rotation);
                g.SetActive(false);
                g.GetComponent<Rigidbody>().velocity = this.rigidbody.velocity + w.firePoint.transform.forward * 100;
                /*
                Vector3 direction = Vector3.zero;
                float dot = Vector3.Dot(this.rigidbody.velocity, w.firePoint.transform.forward);
                if(dot > 0) { direction += this.rigidbody.velocity; }
                */
                g.SetActive(true);
                //Debug.Log("Pew Pew");
            }
        }

        public void Camera() {
            Camera_Rotate();
        }
        private void Camera_Rotate() {
            //this.camera.transform.Rotate(inputs.move_rotation);
        }
    }
    [SerializeField] private Controls controls;

    [SerializeField] private GameObject bullet;
    [SerializeField] private List<Weapon> weapons;
    [SerializeField] private ParticleSystem trail;

    [SerializeField] private Camera camera;

    [System.Serializable]
    private class UI {
        private Inputs inputs;
        private GameObject ui1;
        private GameObject ui2;
        private float timer;
        private List<Weapon> weapons;
        private Text crosshairs;
        private Slider heading_Pitch;
        private Slider heading_Yaw;
        private Slider heading_Roll;
        private Vector3 initialHeading;
        private GameObject gameObject;

        public UI(Inputs i, GameObject u1, GameObject u2, List<Weapon> w, Text c, Slider p, Slider y, Slider r, Vector3 h, GameObject g) {
            this.inputs = i;
            this.ui1 = u1;
            this.ui2 = u2;
            this.timer = 5f;
            this.weapons = w;
            this.crosshairs = c;
            this.heading_Pitch = p;
            this.heading_Yaw = y;
            this.heading_Roll = r;
            this.initialHeading = h;
            this.gameObject = g;
        }

        public void DisplayControls() {
            if (this.timer > 0) {
                this.timer -= Time.fixedDeltaTime;
                if (this.inputs.menu) { timer = -1f; }
            }
            else if (this.timer < 0) {
                this.timer = 0f;
                this.ui1.SetActive(false);
                this.ui2.SetActive(true);
            }
            else {
                if (this.inputs.menu) { this.ui1.SetActive(true); }
                else if (this.ui1.activeSelf) { this.ui1.SetActive(false); }
            }
        }

        public void DisplayHeadings() {
            float pitch = this.initialHeading.x + this.gameObject.transform.rotation.eulerAngles.x;
            if(pitch >= 180) { pitch -= 360; }
            this.heading_Pitch.value = -pitch;

            float yaw = this.initialHeading.y + this.gameObject.transform.rotation.eulerAngles.y;
            if (yaw >= 180) { yaw -= 360; }
            this.heading_Yaw.value = yaw;

            float roll = this.initialHeading.z + this.gameObject.transform.rotation.eulerAngles.z;
            if (roll >= 180) { roll -= 360; }
            this.heading_Roll.value = roll;
        }
    }
    [SerializeField] UI ui;

    [SerializeField] private GameObject menu;
    [SerializeField] private GameObject submenu;
    [SerializeField] private Text crosshairs;
    [SerializeField] private Slider sliderPitch;
    [SerializeField] private Slider sliderYaw;
    [SerializeField] private Slider sliderRoll;
    [SerializeField] private Vector3 initialHeading;

    [System.Serializable]
    private class Utility {
        private Inputs inputs;
        private GameObject light;
        private List<Weapon> weapons;
        private Text crosshairs;
        private RaycastHit hitInfo;
        private Vector3 raycastPoint;
        private Rigidbody rigidbody;
        private float raycastRange;
        private Rigidbody momentumLock;

        public Utility(Inputs i, GameObject l, List<Weapon> w, Text c, Vector3 p, Rigidbody rb, float r, Rigidbody ml) {
            this.inputs = i;
            this.light = l;
            this.weapons = w;
            this.crosshairs = c;
            this.rigidbody = rb;
            this.raycastRange = r;
            this.momentumLock = ml;
        }

        public void Use() {
            if (this.inputs.light != this.inputs.lightOff) { this.light.SetActive(true); }
            else { this.light.SetActive(false); }
        }

        public void Sense() {
            if (ProjectAttack()) { this.crosshairs.color = Color.green; }
            else { this.crosshairs.color = Color.blue; }
            Raycast();
        }
        private bool ProjectAttack() {
            //Debug.Log("ProjectAttack()");
            foreach (Weapon w in this.weapons) {
                RaycastHit hitInfo;
                bool hit = Physics.Raycast(w.firePoint.transform.position, w.firePoint.transform.forward, out hitInfo, w.range);
                if (hit) { return true; }
                //Debug.DrawLine(w.firePoint.transform.position, w.firePoint.transform.position + w.firePoint.transform.forward * w.range, Color.yellow);
            }
            return false;
        }
        private void Raycast() {
            Physics.Raycast(this.raycastPoint, this.raycastPoint - this.rigidbody.transform.position, out this.hitInfo, this.raycastRange);
        }

        public void MomentumLock() {
            this.momentumLock = this.hitInfo.rigidbody;
            if(inputs.momentumLock) {
                Vector3 deltaVelocity = this.momentumLock.velocity - this.rigidbody.velocity;
                this.rigidbody.velocity += deltaVelocity * Time.fixedDeltaTime; // TODO
            }
        }
    }
    [SerializeField] Utility utility;

    [SerializeField] GameObject light;

    //==================== Unity Functions ====================//

    [SerializeField] private Rigidbody rigidbody;

    // Start is called before the first frame update
    void Start() {
        this.rigidbody = this.gameObject.GetComponent<Rigidbody>();

        this.inputs = new Inputs();
        this.controls = new Controls(this.inputs, this.rigidbody, this.bullet, this.weapons, this.trail, this.camera);
        this.ui = new UI(this.inputs, this.menu, this.submenu, this.weapons, this.crosshairs, this.sliderPitch, this.sliderYaw, this.sliderRoll, this.initialHeading, this.gameObject);
        this.utility = new Utility(this.inputs, this.light, this.weapons, this.crosshairs, this.sensePoint, this.rigidbody, this.senseRange,
            this.momentumLock);

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        this.initialHeading = this.gameObject.transform.rotation.eulerAngles;
    }

    [SerializeField] Vector3 sensePoint;
    [SerializeField] float senseRange;
    [SerializeField] Rigidbody momentumLock;

    // Update is called once per frame
    void Update() {
        this.inputs.CaptureInput();
    }

    private void FixedUpdate() {
        this.controls.Move();
        this.controls.Attack();
        this.controls.Camera();
        this.ui.DisplayControls();
        this.ui.DisplayHeadings();
        this.utility.Use();
        this.utility.Sense();

        DebugMode();
    }

    public void DebugMode() {
        //Debug.DrawLine(this.gameObject.transform.position, this.gameObject.transform.forward * 5, Color.blue);
        //Debug.DrawLine(this.rigidbody.transform.position, this.rigidbody.transform.forward * 5, Color.red);
        Debug.DrawLine(this.rigidbody.transform.position, this.rigidbody.transform.position + this.rigidbody.velocity, Color.green);
    }
}
