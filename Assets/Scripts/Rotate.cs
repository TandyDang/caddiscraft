﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    //[SerializeField] Rigidbody rigidbody;
    [SerializeField] Vector3 rotation;
    // Start is called before the first frame update
    void Start()
    {
        //rigidbody.angularVelocity = rotation;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate() {
        transform.rotation *= Quaternion.Euler(rotation * 0.01f);
    }
}
